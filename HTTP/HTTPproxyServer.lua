local Connections = {}
local ConnId = 0

local SecurityHeaders = {
   ["X-Frame-Options"] = "deny",
   ["X-XSS-Protection"] = "1; mode=block",
   ["X-Content-Type-Options"] = "nosniff",
   ["Content-Security-Policy"] = "script-src 'self'",
   ["X-Permitted-Cross-Domain-Policies"] = "none",
--   ["Strict-Transport-Security"] = "max-age=31536000 ; includeSubDomains"
}

local ResponseCode = {
   [200] = "200 OK",
   [301] = "301 Moved Permanently",
   [400] = "400 Bad Request",
   [401] = "401 Unauthorized",
   [403] = "403 Forbidden",
   [404] = "404 Not Found",
   [500] = "500 Internal Server Error"
}

local function parse_header(buffer)
   local found, header_size = buffer:find("\r\n\r\n")
   -- need more data
   if not found then return nil, 0 end
   -- create message
   local message = { data = buffer:sub(1, header_size), header = {} }
	-- parse header
   local header_strings = buffer:sub(1, found-1):split("\r\n")

   for i,v in ipairs(header_strings) do
      if i == 1 then -- response line
         message.method, message.path, message.version = v:match("(%w+) ([^%s]+) HTTP/(%d\.%d)")
         if message.method == nil then
            error("bad HTTP request line: " .. v .. "\nBuffer:\n" .. buffer:sub(1,1024))
         end
      else -- headers
         local name, value = v:match("(.-):%s*(.+)")
         if name == nil then
            error("bad HTTP request header: " .. v .. "\nBuffer:\n" .. buffer:sub(1,1024))
         end
         message.header[name] = value
      end
   end

   if message.header.Cookie then
      local table = {}
      local cookies = message.header.Cookie:split("; ")
      for _,v in ipairs(cookies) do
         local name, value = v:match("([^%s]-)=(.*)")
         table[name] = value
      end
      message.cookies = table
   end
   return message, header_size
end

local function parse_body(object)
   local message = object.message
   local body_size = tonumber(message.header["Content-Length"])
   if body_size <= 0 then
      error("bad HTTP body size: " .. body_size .. "\nBuffer:\n" .. object.buffer:sub(1,1024))
   end
   if body_size > #object.buffer then
      -- need more data
      return false, 0
   end
   -- Transfer-Encoding not supported
   assert(not message.header["Transfer-Encoding"])
   message.body = object.buffer:sub(1, body_size)
   message.data = message.data .. message.body
   -- TODO: apply content enconding
   object.buffer = object.buffer:sub(body_size+1)
   object.state = "header"
   return true
end

local function parse_chunk(object)

   local function implementation(buffer, start, message)
      local size = buffer:match("([%a%d]+)\r\n", start)
      if not size then
         return false, 0
      end
      local length = tonumber(size, 16)
      local position = start + #size + 2
      if position + length + 1 > #buffer then
         return false, 0
      end
      if not message.body then
         message.body = ""
      end
      message.body = message.body .. buffer:sub(position, position + length - 1)
      return true, #size + 2 + length + 2
   end

   local position = 1
   local success, amount
   repeat
      success, amount = implementation(object.buffer, position, object.message)
      position = position + amount
      -- TODO: on_chunk
      if success and amount == 5 then
         -- last chunk
         object.buffer = object.buffer:sub(position + 1)
         object.state = "header"
         return true
      end
   until not success
   object.buffer = object.buffer:sub(position)
   -- need more chunks
   return false
end

local function parse_buffer(object)
   trace(object.state)
   if object.state == "header" then
      local message, amount = parse_header(object.buffer)
      if not message then
         -- need more data
         return false
      end
      object.message = message
      object.buffer = object.buffer:sub(amount+1)
      trace(message)
      if message.header["Content-Length"] then
         object.state = "body"
      elseif message.header["Transfer-Encoding"] and
         message.header["Transfer-Encoding"]:find("chunked") then
         object.state = "chunk"
      else
         -- no body
         return true
      end
      -- parse body, chunked or not
      return parse_buffer(object)
   elseif object.state == "body" then
      return parse_body(object)
   elseif object.state == "chunk" then
      return parse_chunk(object)
   end
end

local function parse_request(self, data)
   -- append new data to internal buffer
   self.buffer = self.buffer .. data
   local success = parse_buffer(self)
   if success then
      ConnId = self.conn_id
      self.on_request(self.message.data)
      -- may be we have another request in the buffer?
      parse_request(self, "")
   end
end

local function parse_request_uri(uri)
   local result = {uri = uri}
   local found = uri:find("?")
   if found then
      result.path = uri:sub(1, found - 1)
      local success = uri:find("#", found)
      if success then
         result.query = uri:sub(found + 1, success - 1)
         result.fragment = uri:sub(success + 1)
      else
         result.query = uri:sub(found + 1)
      end
      for i,v in ipairs(result.query:split("&")) do
         local name, value = v:match("(.-)=(.*)")
         result[name] = value
      end
   else
      result.path = uri:sub(1)
   end
   -- TODO: undo url encode
   return result
end

local function generate_response(Response)
   if not Response.code then
      -- default 200 OK
      Response.code = 200
   end
   local rows = {}
   table.insert(rows, "HTTP/1.1 " .. ResponseCode[Response.code])

   if Response.headers then
      -- inject security header
      for k,v in pairs(SecurityHeaders) do
         Response.headers[k] = v
      end
      for k,v in pairs(Response.headers) do
         -- Strip out the chunked transfer encoding if the response from the server 
         -- contains it since we send the response with content-length
         if k ~= "Transfer-Encoding" and k ~= "transfer-encoding" then
            rows[#rows+1] = k .. ": " .. v
         end
      end

      if not Response.headers["Content-Type"] then
         rows[#rows+1] = "Content-Type: text/html"
      end
   else
      rows[#rows+1] = "Content-Type: text/html"
   end

   if not Response.body then
      Response.body = "<body><h1>" .. ResponseCode[Response.code] .. "</h1></body>"
   end
   assert(type(Response.body) == "string")
   rows[#rows+1] = "Content-Length: " .. tostring(#Response.body)
   rows[#rows+1] = ""
   rows[#rows+1] = Response.body
   return table.concat(rows, "\r\n")
end

local function new_parser(connId, onRequest)
   return {
      conn_id = connId,
      buffer = "",
      state = "header",
      on_request = onRequest,
      parse = parse_request
   }
end

local function respond(Response)
   local Data = generate_response(Response)
   if 0 == ConnId then error ("No Connection ID", 2) end
   socket.send_a(Data, ConnId)
end

local function parse_request2(data)
   if string.find(data, "favicon.ico") == nil then iguana.log(data) end
   local Header = {}
   local HeaderString, Body = unpack(data:split("\r\n\r\n"))
   local HeaderTable = HeaderString:split("\r\n")
   local Method, Path, version
      for i,v in ipairs(HeaderTable) do
      if i == 1 then -- response line
         Method, Path, Version = v:match("(%w+) ([^%s]+) HTTP/(%d\.%d)")
      else -- headers
         local name, value = v:match("(.-):%s*(.+)")
         Header[name] = value
      end
   end
   local Tmessage = {method=Method, path=Path, body=Body, header=Header}
   return Tmessage
end

local function start(Config)
   local Url = "127.0.0.1:" .. Config.port .. "/"
   if "" == Config.ssl.cert or "" == Config.ssl.key then
      Config.ssl = nil
      Url = "http://" .. Url
   else
      Url = "https://" .. Url
   end
   iguana.log(json.serialize{data=Config})
   socket.listen_a(Config)

   local WebHtmlBlock = [[
   <div class='FLDhtmlBlockRow'>
   <div class='FORMlabel FLDrowName'>Listening on:</div>
   <a class='FLDhtmlBlockStyle' target='_blank' href=']]..Url.."'>"..Url..[[</a>
   </div>
   ]]
   component.setStatus{data=WebHtmlBlock}
end

socket.onAccept = function(Address, Id)
   iguana.log("Established new connection " .. tostring(Id) .. " from " .. Address)
   if Connections[Id] then
      error("New connection has the same Id as an existing connection")
   end
   Connections[Id] = new_parser(Id, main)
end

socket.onData = function(Data, Id)
   iguana.log("Received data from connection " .. tostring(Id) .. "\n")
   if not Connections[Id] then
      error("Connection not found")
   end
   Connections[Id]:parse(Data)
end

socket.onWrite = function(Id)
   iguana.log("Connection " .. tostring(Id) .. " is ready to receive more data")
end

socket.onClose = function(Data, Id)
   if #Data > 0 then
      iguana.log("Connection " .. tostring(Id) .. " closed with non-empty buffer: " .. Data)
   else
      Connections[Id] = nil
   end
end

socket.onError = function(Data, Id)
   iguana.log("Connection " .. tostring(Id) .. " encountered error: " .. Data)
end

return {
   start = start,
   respond = respond,
   parseRequest = parse_request2,
   parseRequestUri = parse_request_uri
}
