local HTTPutils = require 'HTTP.HTTPutils'
local postArgs  = require 'HTTP.HTTPclientArgs'

function HTTPconstructGetRequest(urlT, data)
   local getRequest = {}
   table.insert(getRequest, "GET ")
   table.insert(getRequest, urlT.endpoint)

   local iterator = 1
   for i,v in pairs(data.parameters) do
      local Query = {}
      if(iterator == 1) then table.insert(Query, "?")
      else table.insert(Query, "&") end
      table.insert(Query, filter.uri.enc(i))
      table.insert(Query, "=")
      table.insert(Query, filter.uri.enc(v))
      table.insert(getRequest, table.concat(Query))
      iterator = iterator + 1
   end

   table.insert(getRequest, " HTTP/1.1\r\n")
   table.insert(getRequest, "Host: "..urlT.host.."\r\n")
   table.insert(getRequest, "User-Agent: productX\r\n")
   table.insert(getRequest, "Accept: */*\r\n")

   if(data.headers) then
      for i,v in pairs(data.headers) do
         table.insert(getRequest, i)
         table.insert(getRequest, ": ")
         if(type(v) == "table") then table.insert(getRequest, table.concat(v, "; "))
         else table.insert(getRequest, v) end
         table.insert(getRequest, "\r\n")
      end
   end
   table.insert(getRequest, "\r\n")
   return table.concat(getRequest)
end

local function formatData(data)
   if(data == nil)            then data            = {} end
   if(data.parameters == nil) then data.parameters = {} end
   if(data.headers    == nil) then data.headers    = {} end
   if(data.body       == nil) then data.body       = "" end
   return data
end

local function splitResponseHeaderBody()
   local Header = ""
   local Body = ""
   local HeaderParsed = false
   while(not HeaderParsed) do
      Header = Header .. socket.receive()
      local headerStart, headerEnd = Header:find("\r\n\r\n")
      if (headerEnd ~= nil) then
         HeaderParsed = true
         Body = Header:sub(headerEnd + 1)
         Header = Header:sub(1, headerStart - 1)
      end
   end
   return Header, Body
end

local function responseIsChunked(header)
   local FoundIdx = header:find("[tT]ransfer%-[eE]ncoding: chunked")
   return FoundIdx ~= nil
end

local function bodyLength(Header)
   local ad, FoundIdx = Header:find("[Cc]ontent%-[Ll]ength: ")
   local str = Header:sub(FoundIdx+1)
   local endIdx = str:find("\r\n")
   local length = str:sub(0,endIdx)
   return tonumber(length)
end

function HTTPparseChunk(buffer, Out)
   local function implementation(buffer, start, message)
      local size = buffer:match("([%a%d]+)\r\n", start)
      if not size then
         return false, 0, message
      end
      local length = tonumber(size, 16)
      local position = start + #size + 2
      if position + length + 1 > #buffer then
         return false, 0, message
      end
      message = message .. buffer:sub(position, position + length - 1)
      return true, #size + 2 + length + 2, message
   end

   local position = 1
   local success, amount
   repeat
      success, amount, Out = implementation(buffer, position, Out)
      position = position + amount
      if success and amount == 5 then
         -- last chunk
         buffer = buffer:sub(position + 1)
         return true, buffer, Out
      end
   until not success
   buffer = buffer:sub(position)
   -- need more chunks
   return false, buffer, Out
end

function HTTPhandleChunkedBody(Body)
   local HaveAllData = false;
   local Out = ""
   repeat
      HaveAllData, Body, Out = HTTPparseChunk(Body, Out)
      if(not HaveAllData) then
         Body = Body .. socket.receive()
      end
   until(HaveAllData)
   return Out
end

function HTTPhandleNotChunkedBody(Header, Body)
   local bodyLen =  bodyLength(Header)
   while(#Body < bodyLen) do
      Body = Body .. socket.receive()
   end
   return Body
end

function HTTPgetCodeAndHeaders(Header)
   local ReturnCode = Header:match("%d%d%d") -- Http/1.1 200 OK \r\n
   local StatusLineStart, StatusLineEnd = Header:find("\r\n")
   local HeaderStrings = Header:sub(StatusLineEnd + 1)
   HeaderStrings = HeaderStrings:split("\r\n")
   local HeaderTable = {}
   for i,v in ipairs(HeaderStrings) do
      local Name, Value = v:match("(.-):%s*(.+)")
      if Name == nil then
         error("bad HTTP request header: " .. v)
      end
      HeaderTable[Name] = Value
   end
   return ReturnCode, HeaderTable
end

function HTTPhandleResponse()
   local Header, Body = splitResponseHeaderBody()
   if(responseIsChunked(Header)) then
      Body = HTTPhandleChunkedBody(Body)
   else
      Body = HTTPhandleNotChunkedBody(Header, Body)
   end
   local Code, HeaderTable = HTTPgetCodeAndHeaders(Header)
   return Body, Code, HeaderTable
end

function HTTPget (args)
   args            = postArgs.txGetArgs(args)
   local data      = {}
   data.headers    = args.headers
   data.parameters = args.parameters
   local live      = args.live
   data = formatData(data)
   local getRequest = HTTPconstructGetRequest(args.urlT, data)
   if (not args.live and iguana.isTest()) then
      return "To receive data, run with live = true" , "-1", {}, getRequest
   end

   socket.connect{host=args.urlT.host, port=args.urlT.port, secure= args.urlT.scheme == 'https'}
   iguana.log(getRequest)
   socket.send(getRequest)
   local Body, Code, HeaderTable = HTTPhandleResponse()
   socket.close()
   return Body, Code, HeaderTable
end

function HTTPconstructPostRequestNoBody(urlT, data)
   local postRequest = {}
   table.insert(postRequest, "POST ")
   table.insert(postRequest, urlT.endpoint)
   table.insert(postRequest, " HTTP/1.1\r\n")
   table.insert(postRequest, "Host: "..urlT.host.."\r\n")
   table.insert(postRequest, "User-Agent: productX\r\n")
   table.insert(postRequest, "Accept: */*\r\n")
   table.insert(postRequest, "Content-Type: application/x-www-form-urlencoded\r\n")
   if data.headers then
      for i,v in pairs(data.headers) do
         table.insert(postRequest, i)
         table.insert(postRequest, ": ")
         if(type(v) == "table") then table.insert(postRequest, table.concat(v, "; "))
         else table.insert(postRequest, v) end
         table.insert(postRequest, "\r\n")
      end
   end
   local postBody = {}
   for i,v in pairs(data.parameters) do
      table.insert(postBody, filter.uri.enc(i) .. "=" .. filter.uri.enc(v))
   end
   postBody = table.concat(postBody, "&")
   table.insert(postRequest, "Content-Length: " .. #postBody .. "\r\n")
   table.insert(postRequest, "\r\n")
   table.insert(postRequest, postBody)
   return table.concat(postRequest)
end


function HTTPconstructPostRequestWithBody(urlT, data)
   local postRequest = {}
   table.insert(postRequest, "POST ")
   table.insert(postRequest, urlT.endpoint)
   local iterator = 1
   for i,v in pairs(data.parameters) do
      local Query = {}
      if(iterator == 1) then table.insert(Query, "?")
      else table.insert(Query, "&") end
      table.insert(Query, filter.uri.enc(i))
      table.insert(Query, "=")
      table.insert(Query, filter.uri.enc(v))
      table.insert(postRequest, table.concat(Query))
      iterator = iterator + 1
   end
   table.insert(postRequest, " HTTP/1.1\r\n")
   table.insert(postRequest, "Host: "..urlT.host.."\r\n")
   table.insert(postRequest, "User-Agent: productX\r\n")
   table.insert(postRequest, "Accept: */*\r\n")
   table.insert(postRequest, "Content-Type: application/json\r\n")
   table.insert(postRequest, "Content-Length: " .. #data.body .. "\r\n")
   if(data.headers) then
      for i,v in pairs(data.headers) do
         table.insert(postRequest, i)
         table.insert(postRequest, ": ")
         if(type(v) == "table") then table.insert(postRequest, table.concat(v, "; "))
         else table.insert(postRequest, v) end
         table.insert(postRequest, "\r\n")
      end
   end
   table.insert(postRequest, "\r\n")
   table.insert(postRequest, data.body)
   return table.concat(postRequest)
end

function HTTPpost(args)
   args            = postArgs.txPostArgs(args)
   local data      = {}
   data.headers    = args.headers
   data.body       = args.body
   data.parameters = args.parameters
   local live      = args.live
   trace(args)
   data = formatData(data)
   local postRequest = ""
   if #data.body > 0 then
      postRequest = HTTPconstructPostRequestWithBody(args.urlT, data)
   else
      postRequest = HTTPconstructPostRequestNoBody(args.urlT, data)
   end
   if (not args.live and iguana.isTest()) then
      return "To receive data, run with live = true" , "-1", {}, postRequest
   end
   iguana.log(postRequest)
   socket.connect{host=args.urlT.host, port=args.urlT.port, secure=args.urlT.scheme == 'https'}
   socket.send(postRequest)
   local Body, Code, HeaderTable = HTTPhandleResponse()
   socket.close()
   return Body, Code, HeaderTable
end
