require "HTTP.HTTPclient"

local Configs = component.fields()
local BaseUrl = Configs.Host
local Live = false


local function getCookie()
   local _, _, headers = HTTPpost{
      url = BaseUrl .. "/session/login",
      parameters = {
         username = Configs.Username,
         password = Configs.Password
      },
      live = Live
   }
   local cookie = headers["Set-Cookie"]
   if not cookie then
      error("Failed to create a session", 2)
   end
   return cookie:split(";")[1]
end

local function logOut(Cookie)
   local _, _, headers = HTTPpost{
      url = BaseUrl .. "/session/logout",
      headers = {
         cookie = Cookie
      },
      live = Live
   }
   local cookie = headers["Set-Cookie"]
   if not cookie then
      error("Failed to log out", 2)
   end
   local cookie = cookie:split(";")[1]
   if cookie:split("=")[2] ~= "" then
      iguana.logError("Fail to log out")
   end
end

local function createComponent(Name, Cookie)
   local response, code, headers = HTTPpost{
      url = BaseUrl .. "/translator_config/add_component",
      headers = {
         cookie = Cookie
      },
      parameters = {
         created_by = Configs.Username,
         name = Name,
         href = Configs.Template,
         label = Name .. " #tool #generated",
         local_copy = true
      },
      live = Live
   }
	if code ~= 200 then
      error("Failed to create component")
   end
   local res = json.parse(response)
   if not res.success then
      error("Failed to create component: " .. res.issue)
   end
   return res.guid
end

local function linkComponents(Sources, Destination, Cookie)
   local response, code, headers = HTTPpost{
      url = BaseUrl .. "/add_component_source",
      headers = {
         cookie = Cookie
      },
      body = json.serialize{
         data = {
            source_guid = Sources,
            translator_guid = Destination
         },
         compact = true
      },
      live = Live
   }
   if code ~= 200 then
      error("Failed to link components")
   end
   local res = json.parse(response)
   if not res.links_added then
      error("Failed to link components")
   end
end

function main()
   local cookie = getCookie()
   --
   local sources = {}
   for i = 1, Configs.Count do
      table.insert(sources, createComponent("Hammer " .. i, cookie))
   end
   local destination = createComponent("Nail", cookie)
   linkComponents(sources, destination, cookie)
   --]]
   logOut(cookie)
end

main()
